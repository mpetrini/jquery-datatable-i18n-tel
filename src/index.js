/**
 * @requires DataTables 1.10+
 */

// UMD
(function(factory) {
    /* istanbul ignore next */
    if (typeof module === "object" && module.exports) {
        module.exports = factory(require("jquery"), window, document);
    } else if (typeof define === "function" && define.amd) {
        define(["jquery"], function($) {
            factory($, window, document);
        });
    } else factory(jQuery, window, document);
}
(function( $, window, document ) {
    function getCountryData(countryCode) {
        const countryList = window.intlTelInputGlobals.getCountryData();
        for (let i = 0; i < countryList.length; i++) {
            if (countryList[i].iso2 === countryCode) {
                return countryList[i];
            }
        }
        throw new Error(`No country data for '${countryCode}'`);
    }

    function isLocaleNumber(phoneNumber, locale) {
        var nationalDialCode = getCountryData(locale).dialCode;
        return phoneNumber.substring(0, nationalDialCode.length+1) === '+'+nationalDialCode;
    }

    $.fn.dataTable.render.intlPhone = function ( locale ) {
        return function ( data ) {
            if (typeof intlTelInputUtils == 'undefined' || typeof intlTelInputUtils.formatNumber == 'undefined') {
                return data;
            }

            return data != null && data.length > 0 ?
                intlTelInputUtils.formatNumber(
                    data,
                    locale,
                    isLocaleNumber(data, locale) ?
                        intlTelInputUtils.numberFormat.NATIONAL :
                        intlTelInputUtils.numberFormat.INTERNATIONAL
                ) :
                '';
        };
    }
}));
