import * as $ from 'datatables.net-dt';
$.fn = {
    dataTable: $.default()
};

require('./../src/index');
require('./../node_modules/intl-tel-input/build/js/intlTelInput');
require('./../node_modules/intl-tel-input/build/js/utils');

test("core", () => {
    expect($.fn.dataTable.render.intlPhone("fr")("+33102030405")).toEqual("01 02 03 04 05");
    expect($.fn.dataTable.render.intlPhone("fr")("+16502530000")).toEqual("+1 650-253-0000");
});

test("Without Utils", () => {
    var temp = intlTelInputUtils;
    intlTelInputUtils = undefined;
    expect($.fn.dataTable.render.intlPhone("fr")("+33102030405")).toEqual("+33102030405");
    intlTelInputUtils = temp;
});

test("Wrong locale", () => {
    expect(() => {
        $.fn.dataTable.render.intlPhone("fre")("+33102030405")
    }).toThrow();
});

test("Empty phone", () => {
    expect($.fn.dataTable.render.intlPhone("fr")(null)).toEqual("");
});
