var gulpfile = require("gulp");
var babel = require("gulp-babel");

gulpfile.task("build", function() {
    return gulpfile.src(["src/*.js"])
        .pipe(babel({presets: ["@babel/env"]}))
        .pipe(gulpfile.dest("dist"));
});
